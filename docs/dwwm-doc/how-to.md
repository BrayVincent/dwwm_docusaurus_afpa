---
sidebar_position: 4
title: Mettre à jour la documentation
---

````tree
📦docs
 ┣ 📂dwwm-doc
 ┃ ┣ 📂img
 ┃ ┣ 📂ticket-mvp
 ┃ ┃ ┣ 📂img
 ┃ ┃ ┣ 📜_category_.json
 ┃ ┃ ┣ 📜access-control.md
 ┃ ┃ ┣ 📜archivage.md
 ┃ ┃ ┣ 📜crud-user.md
 ┃ ┃ ┣ 📜email.md
 ┃ ┃ ┣ 📜export.md
 ┃ ┃ ┣ 📜flash.md
 ┃ ┃ ┣ 📜general.md
 ┃ ┃ ┣ 📜history.md
 ┃ ┃ ┣ 📜i18n.md
 ┃ ┃ ┣ 📜localisation.md
 ┃ ┃ ┣ 📜logs.md
 ┃ ┃ ┣ 📜password.md
 ┃ ┃ ┣ 📜ticket-details.md
 ┃ ┃ ┣ 📜tutorial.md
 ┃ ┃ ┗ 📜workflow.md
 ┃ ┣ 📜_category_.json
 ┃ ┣ 📜cheat-sheet.md
 ┃ ┣ 📜example.md
 ┃ ┣ 📜how-to.md
 ┃ ┗ 📜syntaxe.md
 ┣ 📂tutorial-basics
 ┣ 📂tutorial-extras
 ┗ 📜intro.md```
````
