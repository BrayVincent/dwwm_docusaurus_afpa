---
sidebar_position: 3
title: Détails Tickets
---

import styles from '../styles.module.css';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import BrowserWindow from '@site/src/components/BrowserWindow';

@Author Ludovic

# Détails Tickets

:::note précisions

Affichage du détail des tickets de l'API MVP créer via Symfony.

:::

## Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) <span className="red-text">fortement recommandé</span>
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI


:::caution vérification
Vérifier ensuite la bonne installation de l'ensemble des composants.

```bash
  Dernier Versionning commun du 25 Mai 2022 ou superieur.
```

:::

## Sur quels fichiers interagir

Afin de vous faciliter la mise en place du détail Ticket, assurez-vous d'ouvrir les fichiers suivants :

<Tabs>
  <TabItem value="Index" attributes={{className: [styles.vscode, styles.ico_tab] }} label="Index.html.twig" default>
    Pour la mise en place des liens sur chaque lignes de la table Ticket.
  </TabItem>
  <TabItem value="TicketController" attributes={{className: [styles.vscode, styles.ico_tab] }} label="TicketController.php">
    Permet la mise en place de la fonction d'affichage du détail des tickets.
  </TabItem>
  <TabItem value="Details" attributes={{className: [styles.vscode, styles.ico_tab] }} label="Detail.html.twig">
    Pages a créer dans le dossier : Templates/ticket, permettra l'affichage des infos du ticket.
  </TabItem>
</Tabs>


## Modifications

Modification a effectuer dans les différents fichier :

<Tabs>
  <TabItem value="IndexT" label="Index.html.twig" default>

```html
<div class="container">
    <h1 class="my-3">Liste des Tickets</h1>
        // This will success
        <center><a href="{{ path('ticket_create') }}" class="btn btn-warning">Ajouter</a></center>   
        
                <table class="table table-dark table-striped table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                        ... // Titres de colonnes
                    </thead>
                    <tbody>
                        {% for ticket in tickets %}
                        // This will success
                            <tr class="link" data-href="{{ path('ticket_detail', {'id': ticket.id}) }}" style="cursor: pointer;">
                                <th scope="row">{{ ticket.id }}</th>
                            ... // Déroulement du ticket
                                <td>{{ ticket.isActive }}</td>    
                                <td><a href="{{ path('ticket_delete', {'id': ticket.id}) }}" class="btn btn-danger">Supprimer</a></td>
                                <td><a href="{{ path('ticket_update', {'id': ticket.id}) }}" class="btn btn-success">Modifier</a></td>
                            </tr>
                        {% endfor %}
                    </tbody>
            </table>

```

Modifier le `block body`, et le `tr` du `tbody` en fonction des noms que vous avez attribué.<br />
(⚠️ Attention a nommé la `class` de votre `tr` la fonction JQuery spécifique a l'affichage en aura besoin.)

```JS 
<script>
    $(document).on('click', 'tr.link:not(a)', function(i){
        i.stopPropagation();
        window.location.href =$(this).data('href');                     
    });
```
Integrez la Fonction .Click au script qui permet l'acces au ticket.
Sa particularité et de rendre "Clickable" toute la ligne du tableau en ignorant les deux boutons `Supprimer` et `Modifier` en stoppant la propagation de l'action a l'appartion d'une balise `a href`.

</TabItem>
<TabItem value="Details" label="detail.html.twig">

```html 

{% extends 'base.html.twig' %}

{% block title %}Details Tickets{% endblock %}

{% block stylesheets %}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
{% endblock %}

{% block body %}

    <div class="container">
        <h1 class="my-3">Détail du Ticket</h1>
            <center><a href="{{ path('app_ticket') }}" class="btn btn-light">Retour</a></center>
                <table class="table table-dark table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Objet</th>
                            <th>Date de création</th>
                            <th>Departement</th>
                            <th>Status</th>
                            <th>Commentaire</th>
                            <th>Date de cloture</th>
                            <th></th>
                            <th></th>
                        </tr>    
                    </thead>

                    <tbody>
                        <tr>
                            <td>{{ ticket.id }}</td>
                            <td>{{ ticket.object }}</td>
                            <td>{{ ticket.createdAt | date('d/m/y H:i') }}</td>
                            <td>{{ ticket.department.name }}</td>
                            <td>{{ ticket.isActive }}</td>
                            <td>{{ ticket.comment }}</td>   
                            <td>{{ ticket.finishedAt | date('d/m/y H:i')}}</td>
                            <td><a href="{{ path('ticket_delete', {'id': ticket.id}) }}" class="btn btn-danger">Supprimer</a></td>
                            <td><a href="{{ path('ticket_update', {'id': ticket.id}) }}" class="btn btn-success">Modifier</a></td>     
                        </tr>
                    </tbody>

{% endblock %}

{% block javascripts %}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
        $('#myTable').DataTable();
} );</script>
{% endblock %}

```
Affichage du ticket sous forme de tableau.

</TabItem>
<TabItem value="Controller" label="TicketController.php">

```php
 /**
 * @Route("/details/{id}", name="ticket_detail", requirements={"id"="\d+"})
 */

public function detailTicket(Ticket $ticket) : Response
{
    //dd($ticket);
    return $this->render('ticket/detail.html.twig', ['ticket' => $ticket]);
}

```

Penser a bien spécifiez la Route.

</TabItem>
</Tabs>

### Details Ticket : Structure

Notre entité doit être définie ainsi

|   Field    |        Type         | 
| :--------: | :-----------------: | 
|     id     | int (AutoIncrement) | 
|   object   |    string (255)     | 
| createdAt  |      datetime       | 
| department |    string (255)     | 
|  isActive  |       boolean       | 
|  comment   |        text         |  
| finishedAt |      datetime       | 

## Visuel Ticket

<BrowserWindow padding="0" url="http://localhost:8000">

![welcome-sym](../img/DetailTicket.png)

</BrowserWindow>

### Architecture Final 

<BrowserWindow padding="0" url="http://localhost:8000">

```tree
📦AFPA_TICKETS
 ┣ 📂.vscode
 ┣ 📂bin
 ┣ 📂config
 ┣ 📂migrations
 ┣ 📂public
 ┣ 📂src
 ┃ ┣ 📂Controller
 ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┣ 📜SecurityController.php
 ┃ ┃ ┣ 📜TicketController.php
 ┃ ┣ 📂Entity
 ┃ ┣ 📂Repository
 ┃ ┣ 📂Security
 ┃ ┗ 📜Kernel.php
 ┣ 📂templates
 ┃ ┣ 📂Security
 ┃ ┣ 📂ticket
 ┃ ┃ ┣📜detail.html.twig
 ┃ ┃ ┣📜index.html.twig
 ┃ ┃ ┣📜userForm.html.twig
 ┃ ┗ 📜base.html.twig
 ┣ 📂var
 ┣ 📂vendor
 ┣ 📜.env
 ┣ 📜.env.local
 ┣ 📜.gitignore
 ┣ 📜composer.json
 ┣ 📜composer.lock
 ┗ 📜symfony.lock
```

</BrowserWindow>



