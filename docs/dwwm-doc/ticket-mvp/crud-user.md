---
sidebar_position: 2
title: CRUD User
---

# <span className="titleSize red-text">CRUD User</span>

Si on faisait travailler les autres ?

# <span className="colorB">PHASE 1 : préparer le terrain</span>

Pour ce faire, il nous faut des autres, alors on va les créer.

Et rien de plus simple avec **SYMFONY**.

On commence avec un peu de console :

`composer require validator`

`symfony console make:crud`

![](../img/crud_user.PNG)

> Et c'est gagné

Enfin presque... Il reste quelques tous petits et minimes changements à apporter.

# <span className="colorB">PHASE 2 : un peu de configuration</span>

Là vous ne le savez pas mais vous êtes inconscients, vos mots de passe sont en clair en BDD et <span className="emphase">**c'est mal**!</span>

## <span className="colorG">Hash du Password</span>

On va devoir hasher les mots de passe à la création d'un utilisateur.

Donc dans le UserController, votre fonction new va ressembler à ça:

_(pour les plus perspicaces, c'est la même choses qu'il y avait dans notre fixtures)._

```php
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($this->hasher->hashPassword($user, $form['password']->getData()));
            $userRepository->add($user, true);

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }
```

Et idem pour l'update du profil donc la fonction _edit()_:

```php
if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($this->hasher->hashPassword($user, $form['password']->getData()));
            $userRepository->add($user, true);

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }
```

C'est mieux mais ce n'est pas encore ça. Si mon cher utilisateur utilise son clavier aussi mal que <span className="red-text">Nicolas</span>, il va galérer pour savoir ce qu'il a choisi comme password.

Rajoutons une vérification.

## <span className="colorG">Confirmation du Password</span>

**Rendez-vous dans le UserType, s'il vous plaît.**

Le builder qui gère le formulaire, va devoir être modifié pour la ligne _password_:

```php
    add('password', RepeatedType::class, [
    'type' => PasswordType::class,
    'invalid_message' => 'The password fields must match.',
    'options' => ['attr' => ['class' => 'password-field']],
    'required' => true,
    'first_options'  => ['label' => 'Password'],
    'second_options' => ['label' => 'Repeat Password'],
])
```

> Pensez à importer les class qui vont bien.

## <span className="colorG">Vous n'oubliez rien ?</span>

Bah oui! Y'a pas de rôles pour vos utilisateurs! Vous n'êtes pas les couteaux les plus aiguisés du tiroir...

On retourne dans le **BUILDER** :

```php
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('email', EmailType::class)
            ->add('roles', ChoiceType::class,  [
                'required' => true,
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'ADMIN' => 'ROLE_ADMIN',
                    'USER' => 'ROLE_USER',
                    'CLIENT' => 'ROLE_CLIENT',
                    'TECH' => 'ROLE_TECH',

                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])
            ->add('name');
    }
```

# <span className="colorB">PHASE 2 : juste un dernier TWIG</span>

Ce serait pas mal que la gestion de salariés ne soit accessible qu'aux admins, non?

_si vous avez répondu "non", nous êtes virés._

Sur le index.html.twig, on va ajouter un bouton pour les admins :

```php
{% if app.user.roles[0] == 'ROLE_ADMIN' %}

    <a href="{{path('app_user_index')}}" class="btn btn-primary">Gestion salariés</a>

{% endif %}
```

_Et pour les plus téméraires, vous pouvez ajouter un bouton retour ;)_

@Author Marin, Arnaud
