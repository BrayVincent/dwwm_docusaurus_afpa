---
sidebar_position: 9
title: Home
---

# **Home, Navbar, Dark/Light Mode**

@Author Antoine, Malik

# **Home**


Avec le bundle Maker, entrer la ligne de commande suivante afin de créer le contrôleur gérant la page d'accueil :

> ` symfony console make:controller HomeController `

Vous devriez avoir votre contrôleur dans le dossier Controller et un nouveau dossier dans Templates qui se nomme home :

![alt text](../img/arborescence_maker.png)

il faut redéfinir la route pour arriver sur la page d'accueil, pour cela, il faut aller dans le contrôleur HomeController, puis sur la méthode index, modifier l'annotation pour obtenir la bonne route. Cette dernière doit devenir ` @Route("/{_locale}/", name="app_home") `.  

### **Avant**

![alt text](../img/symfony-5-welcome.png)

Avec l'internationnalisation, il faut une méthode pour éviter la page ci-dessus, on ajoute à notre contrôleur la méthode suivante qui redirige vers la méthode index du contrôleur et forcer la langue de la variable `_locale` : 

```php
	/**
    * @Route("/")
	*/
    public function indexNoLocale(): Response
    {
        return $this->redirectToRoute('app_home', ['_locale' => 'fr']);
    }
```

### **Après**

![alt text](../img/homepage.png)

# Navbar

Pour commencer, dans templates, on crée un nouveau fichier _navbar.html.twig dans le dossier `shared`. On crée un block twig avec le nom `navbar` puis on va récupérer sur bootstrap le code adéquat pour generer une navbar.
 
![alt text](../img/Navbar.png)

Afin que la navbar soit visible sur la totalité du site il faut l'integrer au ficher `base.html.twig`. 

Un controle est mis en place dans le fichier partial `_navbar.html.twig` pour savoir si un utilisateur est connecté. S'il est connecté le dropdown affiche le nom/prénom du User et propose une option de déconnexion qui redirige vers la page Login.
Si aucun user est identifié, l'utilisateur ne pourra se rendre uniquement que sur les pages `index.html.twig` et `login.html.twig`

```html
<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Compte</a>
		<ul class="dropdown-menu">
			{% if app.user %}
				<li><a class="dropdown-item">{{ app.user.Name }}</a></li>
				<li><a class="dropdown-item" href=" {{ path('app_logout')}}">Déconnexion</a></li>
			{% else %}
				<li><a class="dropdown-item">Vous n'êtes pas connecté</a></li>
				<li><a class="dropdown-item" href=" {{ path('app_login')}}">Connexion</a></li>
			{% endif %}
		</ul>

```
[🗃 Télécharger le code Navbar](../assets/_navbar.html.twig)

Pour les redirections les autres boutons de la navbar, le bouton home a pour route : `app_home` et pour le bouton ticket : `app_ticket`.

# **Tableau de bord** 

Afin d'habiller la page d'accueil, nous pouvons mettre en place des graphiques et faire de la page d'accueil un tableau de bord. 
Pour réaliser ces graphiques plus simplement et dynamiquement, il faut passer par la librairie `ChartJs`.

## **Installation de ChartJs**

Pour utiliser la librairie `Chart.Js`
Utilisez le CDN suivant : 

```html
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
```

A mettre dans le fichier `base.html.twig` à la suite des intégrations CDN déjà mis en place.

---

## **Mise en place des graphiques**

Pour créer un graphique, il faut en premier lieu ouvrir une balise `<canvas>` dans le fichier twig ou vous souhaitez l'afficher.

Exemple:

```html
<canvas id="monGraph" style="le style que vous souhaitez"></canvas>
```

Ensuite il faut configurer les données, les labels, la forme du graphique etc... 
Tous ceci se passe en JavaScript. Ouvrez `un block javascripts` à la suite de votre `bloc body`.

### **- 1er étape dans le JS : Récuperer l'id de la balise `<canvas>` créer en amont qui represente le corps de votre graphique sur votre page**

Exemple:

```js
var graph = document.getElementById("monGraph");
```

### **- 2ème étape dans le JS : Configuration du graphique**

La syntaxe de base pour configurer le graphique est la suivante : 

```js
var maVariable = new Chart('monGraph', {
    type: ,
    data: {
        labels: ,
        datasets: [{
            data:[,
            backgroundColor: []
        		}]
        },
options: {}
});
```

Le type = la forme du graphique ('bar', 'pie', 'line' etc... ) [Documentation Charts JS avec tous les types possibles](https://www.chartjs.org/docs/latest/charts/area.html)
La data est composée de plusieurs données à remplir : 
- Le label = le nom que l'on veut attribuer à une valeur
- Le dataset est composé de la données data, qui correspond à nos valeurs et du backgroundColor qui correspond à la couleur à attribuer aux valeurs de nos graphiques.


Code complet pour avoir les graphiques finaux vu précedemment en image :
```html
{% block body %}
	<body>

		<h1 style='margin-top:2%;margin-left:5%'>Tableau de bord</h1>

		<canvas id="myChartActive" style="width:100%;max-width:700px;position:absolute;margin-left:50%;margin-top:3%"></canvas>
		<canvas id="myChartDep" style="width:100%;max-width:700px;position:absolute;margin-left:10%;margin-top:3%"></canvas>

	</body>

{% endblock %}
{% block javascripts %}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script>
    	var graph = document.getElementById("myChartActive");
        var myCharts = new Chart('myChartActive', {
            type: 'pie',
            data: {
                labels: ['Tickets Terminés ', 'Ticket en Cours', 'Wip'],
                datasets: [{
                    data: [{{ countNoActive }}, {{ countActive }}, {{ countWip }}],
                    backgroundColor: ['blue', 'cyan', 'violet']
                            }]
                    },
            options: {}
        });
    
    
        var graph = document.getElementById("myChartDep");
        var myCharts = new Chart('myChartDep', {
            type: 'pie',
            data: {
                labels: [{{ nameDep | raw }}],
                datasets: [{
                    data: [{{ nbTickets | join(',') }}],
                    backgroundColor: ['blue','cyan','pink','violet','brown','orange','red','grey','white','yellow']
                            }]
                 },
            options: {}
        });
    </script>

{% endblock %}
```
---
## **Rendre les graphiques dynamique par rapport à notre base de donnée**

### **1er Graphique**

Rendez vous dans notre fichier `TicketRepository.php` afin de créer une requête pour recuperer les valeurs ainsi que les noms associés et les inserer à nos graphiques.

Pour le premier graphique nous souhaitons mettre en avant les différents états des tickets. Pour cela créer une nouvelle fonction comportant la requête : 

```php
    public function getAllWithStatus($value)
    {
        return $this->createQueryBuilder('t')
            ->where("t.ticket_statut = :val ")
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }
```

Afin d'integrer nos réponses à nos graphiques situés dans le fichier `index.html.twig` nous devons interpreter ces dernières dans notre controleur `HomeController.php`
Rendez-vous dans la fonction index pour integrer nos réponses dans le return qui enverra tous dans notre twig.

```php
		$countActiveTicket = count($this->ticketRepository->getAllWithStatus('initial'));
        $countNoActiveTicket = count($this->ticketRepository->getAllWithStatus('finished'));
        $countWipTicket = count($this->ticketRepository->getAllWithStatus('wip'));

        return $this->render('home/index.html.twig', [
            'countActive' => $countActiveTicket,
            'countNoActive' => $countNoActiveTicket,
            'countWip' => $countWipTicket

        ]);
```

Nous rajoutons un count à nos réponses apportées par la méthode `getAllWithStatus()` pour les convertir en string car la réponse que l'on nous renvoi est un tableau que nous ne pouvons pas interpreter en twig.

Une fois les valeurs `'contActive'`, `'contNoActive'`, `CountWip` envoyées dans notre twig nous devons les integrer dans les options correspondantes de notre graphique.


----
### **2ème Graphique**

Pour le 2ème graphique nous souhaitons récuper les noms des service afin de les integrer au Label de notre graphique et d'afficher le nombre de ticket de chaques services.
Le fait de récuperer plus de donnée d'un coup complexifie le traitement des requêtes pour l'envoyer dans notre twig.

Nous repartons sur le même schéma que le 1er graphique : 
- Création de nos requêtes
- Convertir les réponses dans notre controler
- Les integrer à notre fichier twig

Avec la requête suivante :

```php
    public function getAllDep(){
        return $this->createQueryBuilder('t')
        ->select('COUNT(t.department), (d.name)' )
        ->join('App\Entity\Department', 'd', 'WITH', 't.department = d.id' )
        ->groupBy('t.department')
        ->getQuery()
        ->getResult();
    }
```

Nous récupérons un tableau associatif contenant le noms des services dans une valeur et le nombre de ticket qui lui est associé en dans une seconde valeur.

![alt text](../img/tableau_associatif.png)

Pour convertir notre tableau associatif il faut créer en amont dans notre fonction `index()` du `HomeController` deux variables contenant un tableau vide pour accueillir nos données.

```php 
$tabDep = [];
$tabTickets = [];

	$countDepGroupBy = $this->ticketRepository->getAllDep();

	        foreach($countDepGroupBy as $key=> $tickets){
	            $tabTickets[] = $tickets[1];
	            $tabDep[] = "\"" . $tickets[2] . "\"";  
	        }

	        return $this->render('home/index.html.twig', [
	            'nbTickets' => $tabTickets,
	            'nameDep'=> implode("," , $tabDep),

       		 ]);
```

Nous récupérons notre tableau associatif contenant les données avec la méthode `"getAllDep()"`

Nous remplissons nos tableaux avec une boucle `foreach()`.

Dans la tableau `$tabTickets[]` nous y envoyons des données numériques qui sont plus facilement interprétable par twig sous forme de tableau. Alors que dans le `$tabDep[]` nous y mettons des strings qui nécéssite plus de traitement dans le controleur en y concaténant des doubles quote pour chaque ajout de valeur dans la boucle.
Par la suite il faut passer par un implode lorsque nous envoyons notre tableau `$tabDep[]` dans notre fichier Twig.

Dans notre twig, nous inserons nos tableaux dans les options correspondantes de notre graphique.

Pour interpreter nos tableaux nous devons integrer des `options` dans notre moustache qui contient nos tableaux.
Pour le tableau contenant des valeurs numériques une `option join` est necessaire, avec comme séparateur (',').
Pour le tableau contenant les strings il faut rajouter une `option raw`. car lorsque nous envoyons les tableaux sur twig les doubles quotes sont mal interpretées  

![alt text](../img/console_quote.png)

---

# Light/Dark mode

Tout d'abord dans la navbar, on va ajouter un bouton pour pouvoir changer le thème visuel du site, on lui donne une classe bootstrap et un id. Ensuite dans le fichier `base.html.twig`, on retire le lien de la feuille CSS en supprimant l'attribut href pour ensuite lui donner un id. 

```html
<link id='themeCSS' rel="stylesheet" href= "" >
```
Enfin on ajoute du javascript dans le fichier `_navbar.html.twig` pour garder le choix du mode de l'utilisateur dans le SessionStorage .

```js
	var btnTheme = document.getElementById("theme");
			var link = document.getElementById("link");
			if (link.href == ""){
				sessionStorage.setItem("theme", "https://bootswatch.com/5/cerulean/bootstrap.css");
			}
			link.href = sessionStorage.getItem("theme");
			btnTheme.addEventListener("click", function () {
				if (link.href == "https://bootswatch.com/5/vapor/bootstrap.css"){
					link.href = "https://bootswatch.com/5/cerulean/bootstrap.css";
					sessionStorage.setItem("theme", "https://bootswatch.com/5/cerulean/bootstrap.css");
				}
				else  {
					link.href = "https://bootswatch.com/5/vapor/bootstrap.css";
					sessionStorage.setItem("theme", "https://bootswatch.com/5/vapor/bootstrap.css");
				}
			});
```
Vous pouvez changer les thèmes bootswatch à votre guise en y integrant votre lien ou mettre vos propres feuilles CSS en respectant bien les chemins pour que chacune de vos pages n'ai pas d'erreur.

### Light mode

![alt text](../img/light_mode_home.png)

### Dark mode

![alt text](../img/homepage.png)