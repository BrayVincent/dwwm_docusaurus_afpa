---
sidebar_position: 15
title: Pièces jointe
---
import styles from '../styles.module.css';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import BrowserWindow from '@site/src/components/BrowserWindow';

@Author Ludovic

# Pieces jointe

:::note précisions

Possibilité d'envoi d'enregistrement d'une pieces jointe lors de la création ou la modification d'un ticket.

:::

## Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) <span className="red-text">fortement recommandé</span>
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI


:::caution vérification
Vérifier ensuite la bonne installation de l'ensemble des composants
:::

# Où Stocker notre pieces jointe ?

Commencer par créer un dossier dans `Public` que l'on nommera `uploads`.

Ensuite depuis le terminal lancer la commande : 

```bash
$ git add public/uploads/.gitignore
```

# Installation de bundle

## Etapes 1 Modification de la BDD
```bash
symfony console make:entity
```

Entité `Ticket`, crée une colonne `attachment`  en String, avec 255 caractères, qui peut etre NULL.

Ensuite : 

```bash
symfony console make:migration
```
Puis :
```bash
symfony console doctrine:migration:migrate
```


## Etapes 2 Installation du bundle MIME
```bash
composer require symfony/mime
```
## Etapes 3 Installation de Assets symfony
```bash
composer require symfony/asset
```



# Modification

<Tabs>
  <TabItem value="IndexT" label="detail.html.twig" default>

```html
   
...... // 
        <th>Status</th>
        <th>Commentaire</th>
        <th>Date de cloture</th>
        <th></th>
        <th>Pièces jointes</th>
    </tr>    
...... //
        <td><a href="{{ path('ticket_delete', {'id': ticket.id}) }}" class="btn btn-danger">{{ 'button.delete' | trans }}</a></td>
        <td><a href="{{ path('ticket_update', {'id': ticket.id}) }}" class="btn btn-success">{{ 'button.update' | trans }}</a></td>
        <td><img src="{{asset('uploads/' ~ ticket.attachment)}}" height='50' width='50'></td>
    </tr>
</tbody>

```

Ajout de la colonne pour l'affichage de la piece jointe.


</TabItem>
    <TabItem value="Controller" label="TicketController.php">

Dans la fonction ticket 

```php
public function ticket(Ticket $ticket = null, Request $request) : Response
    {
      //// .....
            $form = $this->createForm(TicketType::class, $ticket, []);

            $form->handleRequest($request);
    // This will success
if ($uploadedFile = $form['attachment']->getData()) {
       // This will success
                $originalFilename = pathinfo                ($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                   // This will success
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                   // This will success
                $newFilename = $safeFilename.'-'.uniqid().'.'.
                   // This will success
                $uploadedFile->guessExtension();
   // This will success
                try {   
                       // This will success
                    $uploadedFile->move(
                           // This will success
                        $this->getParameter('kernel.project_dir').'/public/uploads',
                           // This will success
                        $newFilename
                           // This will success
                    );
                       // This will success
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                       // This will success
                }
   // This will success
                $ticket->setAttachment($newFilename);
                   // This will success
            }
```

⚠️ !! Attention à bien importer les classes !! ⚠️
Mise en place du controller de ticeket por la prise en compte de la piece jointe. La fonction permet :
- De conserver le nom original du fichier qui est envoyé.
- Elle passe le nom dans une regex pour eviter un maximum les injections (rien n'est parfait mais sa aide eviter les petit ennuis)
- Elle modifie la Case en passant le nom en tout minuscule.
- Elle attribue une ID unique au bout du nom du fichier de facon a ne pas provoquer de conflit si plusieur fichier ont le même nom.
- Elle conserve le type d'extension (.png, .jpeg, .gif, pdf...).
Au final votre pieces jointe et envoye dans le dossier public/uploads, puis dans le base de données.

</TabItem>
    <TabItem value="Ticket" label="TicketType.php">

```php

 ->add('attachment', FileType::class, ['label' => 'Pièces Jointes'], ['required' => false])

 ```
Permet de rendre la pieces jointe visible lors de la création de ticket, elle n'est pas requise pour l'envoi du ticket.

 </TabItem>
 </Tabs>



Et voilà vous pouvez maintenant joindre une photo du patron qui se ridiculise à l'apéro de nöel de la boite dans vos tickets.