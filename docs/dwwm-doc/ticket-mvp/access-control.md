---
sidebar_position: 1
title: Access Control
---

import styles from '../styles.module.css';

# Access Control + Propriétaire Tickets

@Author Vanessa, Thibault

## Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) fortement recommandé
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI

### Bundles nécessaires

- DoctrineBundle
- DoctrineFixturesBundle
  <br />
  <br />

## Modification de l’entity « Ticket »

<p>Afin de pouvoir filtrer les tickets par "auteur", il est nécessaire d'ajouter une relation entre les entités Ticket et User.</p>

```bash
symfony console make:entity Ticket
```
![](../img/etape1.png)

## Générer la migration

```bash
 symfony console make:migration
```

<br />

## Modification avant lancement de la migration

Dans le dernier fichier de migration,
ajout de la ligne ci-dessous dans la méthode : `public function up()`

```php
$this->addSql('TRUNCATE TABLE ticket') ;
```

<br />

## Lancer la migration

```bash
symfony console doctrine:migration:migrate --no-interaction
```

<br />

## Création de fixtures

Modifier le fichier src/DataFixtures/***AppFixtures*** :
- en plus des données département,
- on récupère toutes les données de tous les users

```php
$allUsers = $manager->getRepository(User::class)->findAll();
```

pour ensuite nourrir le ticket on lui ajoute un user

```php
 ->setUser($faker->randomElement($allUsers));
```

## Importation des fixtures

```bash
symfony console doctrine:fixtures:load --no-interaction
```

# Affichage des tickets de l'utilisateur connecté

## Récupération des informations user

Dans le fichier ***TicketController.php*** > `function index()`

Afin de récupérer les infos users,
décommenter ou ajouter la ligne suivante :

```php
// **_récupère les infos de l’utilisateur en cours_**
$user = $this->getUser();
```

<br />Modifier la ligne de récupération de tous les tickets par :
```php
// **_récupère les tickets là où l’utilisateur existe_**
$tickets = $this->ticketRepository->findBy(['user' => $user] );
```
Toujours dans le fichier TicketController.php
Pour éviter les soucis de création de ticket il faut également ajouter la ligne suivante dans la `function ticket()` 

```php
$user = $this->getUser();
```

puis nourrir le ticket avec un user 
```php
->setUser($user)
```

### Vérifiez, sur votre page d’accueil après connexion, vous ne devriez avoir que les tickets qui ont été créé par l’utilisateur connecté.
