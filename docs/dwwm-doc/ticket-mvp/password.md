---
sidebar_position: 12
title: Mot de passe oublié
---

# Mot de passe oublié

@Author Quentin

:::info
La fonction **mot de passe oublié** est une fonction particulièrement utile lorsque l'on ne se souvient plus de son mot de passe, son ajout est donc necessaire lors d'une authentification sur une site web.
:::

Tout d'abord il vous faut installer des bundles complementaires qui feront parfaitement le travail pour notre fonction.
# 1er Etape:

Le premier bundle à installer est le bundle mailer pour cela il faut entrer la commande suivante dans votre terminal.

### Code
`composer require symfony/mailer`

Une fois le bundle installé dans le dossier **config/packages** le fichier mailer.yaml sera créer vous n'aurez pas besoin de le modifier par la suite.
Il vous faudra aussi ajouter le bundle suivant celui-ci permet d'envoyer des emails supporter par un fournisseur d'emails.

# 2nd Etape:

Le second bundle à importer et le bundle permettant la reinitialisation du mot de passe dans la base de donnée.


### Code
`composer require symfonycasts/reset-password-bundle`

Il faut ensuite ajoputer le bundle validator.

### Code

`composer require symfony/validator`

Il faut ensuite créer le reset password pour cela on entre une seconde commande dans le terminal a la suite de la premiere.

### Code
`symfony console make:reset-password`

Cela vous pose quelques questions sur votre application et génère tous les fichiers dont vous avez besoin! Ensuite, vous verrez un message de réussite et une liste de toutes les autres étapes que vous devez faire.

Une fois la géneration des fichiers terminés on peut voir que plusieurs types de fichiers sont arriver dans l'arborescence du projet:
- ResetPasswordController.php
- ResetPasswordRequest.php
- check_email.html.twig
- email.html.twig
- request.thml.twig
- reset.html.twig

:::note A faire
Pensez a faire une migration

:::note Pensez-y
Ne pas oublier de créer un bouton sur votre page de connexion qui renvoit sur la page de réinitialisation du mot de passe en indiquant la bonne route en twig avec un jolie **{{path('le jolie chemin de votre choix')}}**
:::

Dans le fichier **ResetPasswordController.php** l'objet mail est créer dans une des fonctions du fichier, c'est ce mail qui sera envoyer quand l'utilisateur fera la demande de réinitialisation de mot de passe avec le lien correspondant dans le mail.

![alt text](./img/mail.PNG)

L'objet **email** est gerer par **MailerInterface**

![alt text](./img/import_de_la_classe_mail_interface.PNG)

Le contenue de l'email est créer a partir du fichier **email.html.twig**:

![alt text](./img/contenue_mail.PNG)

L'email une fois envoyer il faut le récupérer pour pouvoir obtenir le lien necessaire a la modification du mot de passe en base de donnée et pour cela nous allons utiliser l'outil [**Mailtrap**](https://mailtrap.io/), pour utiliser cet outils il est necessaire de posséder un compte l'inscription est gratuite.
Une fois l'inscription effectuée rendez vous dans l'onglet **Testing puis Inboxes**, ensuite dans le menu déroulant pour le format d'intégration dans votre application sélectionner Symfony 5+.

![alt text](./img/integration_mailtrap.PNG)

Copiez le lien, ensuite aller dans votre fichier **.env**, puis remplacer le **MAILER_DSN** de base par le votre.

![at text](./img/mailer_dsn.PNG)

Vous pouvez maintenant récupérer le mail envoyer a l'adresse renseigner par l'utilisateur grace à Mailtrap.

![alt text](./img/exemple_mail_resetPwd.PNG)

Maintenant il vous reste juste à tester tout cela en vérifiant dans votre base de donnée avant le process et après.







