---
sidebar_position: 6
title: Message d'erreur + Flash
---

import styles from '../styles.module.css';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';





# Installation d'une feature Symfony

:::note précisions

Dans ce tuto, nous verrons dans un premier temps la mise en place de pages d'erreurs personnalisées puis la mise en place de message
flash.

:::

## Message d'Erreur

![BRUCE](https://www.helloworkplace.fr/wp-content/uploads/2013/09/clavier.gif)

:::note précisions

Le but est d'afficher un message d'erreur personnalisé pour l'utilisateur qui explique la nature du problème.

:::

## Requirements

Afin de pouvoir mettre en place les messages d'erreurs vous devez vous assurer de réaliser les étapes suivantes :

### Pré-requis

- IDE (VSCode ou PHPStorm recommandé)
- Terminal (iTerm2 sous macOS, Hyper sous Windows recommandé)
- Versionning (Git) <span className="red-text">fortement recommandé</span>
- PHP 7.2.5 minimum (Symfony 5, PHP 8.0.2 -> Symfony 6)
- MySQL
- Composer (Gestionnaire de dépendances PHP)
- Symfony CLI
- Le pack "Twig-pack"

:::caution vérification
Vérifier ensuite la bonne installation de l'ensemble des composants

```bash
  symfony check:requirements
```

:::


## Installation du Twig-Pack

Se déplacer dans le répertoire de travail à l'aide de la ligne de commande

<Tabs groupId="operating-systems">
  <TabItem value="new" label="Composer">

Ajouter le Twig-Pack en utilisant composer

```bash

composer require symfony/twig-pack 

```
</TabItem>
</Tabs>

### Architecture classique d'une application Symfony



```tree
📦application_symfony
 ┣ 📂bin
 ┃ ┗ 📜console
 ┣ 📂config
 ┣ 📂migrations
 ┣ 📂public
 ┃ ┣ 📜.htaccess
 ┃ ┗ 📜index.php
 ┣ 📂src
 ┃ ┣ 📂Controller
 ┃ ┣ 📂Entity
 ┃ ┣ 📂Repository
 ┃ ┗ 📜Kernel.php
 ┣ 📂templates
 ┃ ┗ 📜base.html.twig
 ┣ 📂var
 ┣ 📂vendor
 ┣ 📜.env
 ┣ 📜.env.local
 ┣ 📜.gitignore
 ┣ 📜composer.json
 ┣ 📜composer.lock
 ┗ 📜symfony.lock
```



<Tabs>
  <TabItem value="bin" attributes={{className: [styles.bin, styles.ico_tab] }} label="bin" default>
    Contient le principal point d’entrée de la ligne de commande : console.<br />
    Vous l’utiliserez tout le temps.<br />
  </TabItem>
  <TabItem value="config" attributes={{className: [styles.config, styles.ico_tab] }} label="config">
    Constitué d’un ensemble de fichiers de configurations sensibles, initialisés avec des valeurs par défaut. Un fichier par paquet.<br />
    Vous les modifierez rarement : faire confiance aux valeurs par défaut  est presque toujours une bonne idée ;)
  </TabItem>
  <TabItem value="public" attributes={{className: [styles.public, styles.ico_tab] }} label="public">
    Est le répertoire racine du site web, et le script index.php est le point d’entrée principal de toutes les ressources HTTP dynamiques.
  </TabItem>
  <TabItem value="src" attributes={{className: [styles.src, styles.ico_tab] }} label="src">
    Héberge tout le code que vous allez écrire.<br />
    C’est ici que vous passerez la plupart de votre temps.<br />
    Par défaut, toutes les classes de ce répertoire utilisent le namespace PHP App.<br />
    C’est votre répertoire de travail, votre code, votre logique de domaine.
  </TabItem>
  <TabItem value="templates" attributes={{className: [styles.templates, styles.ico_tab] }} label="templates">
    Héberge tous les templates html.twig que vous allez écrire.<br />
    C’est le répertoire pour gérer votre visuel.<br />
    (⚠️ CSS et JS sont à placer dans le répertoire public/)
  </TabItem>
  <TabItem value="var" attributes={{className: [styles.var, styles.ico_tab] }} label="var">
    Contient les caches, les logs et les fichiers générés par l’application lors de son exécution.<br />
    Vous pouvez le laisser tranquille.<br />
    (Répertoire à supprimer si vous souhaiter faire une copie de votre application)
  </TabItem>
  <TabItem value="vendor" attributes={{className: [styles.vendor, styles.ico_tab] }} label="vendor">
    Contient tous les paquets installés par Composer, y compris Symfony lui-même.<br />
    C’est notre arme secrète pour un maximum de productivité.<br />
    Ne réinventons pas la roue. Vous profiterez des bibliothèques existantes pour vous faciliter le travail.<br />
    Le répertoire est géré par Composer.<br />
    <span className="red-text">N’y touchez jamais. (si vous devez modifier le comportement natif de Symfony créez une extension)</span><br />
    (Répertoire à supprimer si vous souhaiter faire une copie de votre application)
  </TabItem>
  <TabItem value="env" attributes={{className: [styles.env, styles.ico_tab] }} label=".env">
    Fichier de configuration d’environnement de votre application Symfony
  </TabItem>
  <TabItem value="composer" attributes={{className: [styles.composer, styles.ico_tab] }} label="composer.json">
    Liste les bundles installés dans votre application.<br />
    Vous pouvez désinstaller un bundle en le supprimant de ce fichier puis en lançant la commande composer update
  </TabItem>
</Tabs>


## Création des fichiers nécessaires


- Créez dans le dossier templates, un dossier "bundles"
- Dans le dossier "bundles" créez un dossier "TwigBundle"
- Dans le dossier "TwigBundle" créez un dossier "Exception"
- Et pour finir, dans le dossier "Exception" créez les fichiers : "error404.html.twig", "error403.html.twig" et "error.html.twig"

Vous devriez vous retrouver avec l'arborescence suivante : 







![arborescence](../img/arborescence.png)



### Création de la page erreur 404

Ouvrez le fichier "error404.html.twig" et collez le code suivant : 

<Tabs groupId="operating-systems">
  <TabItem value="new" label="VS Code">

```html

{% block stylesheets %}
	{# <link rel="stylesheet" href="//cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css"> #}
 

{% endblock %}
{% block body %}
<section class="page_404">
	<div class="container-fluid">
		<div class="row">	
		<div class="col-sm-12 col-md-12 col-lg-12 ">
		<div class="col-sm-12 col-sm-offset-1  text-center">
		<div class="four_zero_four_bg">
			<h1 class="text-center py-5">Error 404</h1>
		
        <img src="https://cdn.dribbble.com/users/285475/screenshots/2083086/dribbble_1.gif" alt="" srcset="">
		
		</div>
		
		<div class="contant_box_404">
		<h3 class="h2">
		Il semblerait que vous soyez perdu !
		</h3>
		
		<p>La page que vous recherchez n'existe pas !</p>
		
		<a href=" {{ path("app_login") }}" class="link_404">Retour à l'accueil</a>
	</div>
		</div>
		</div>
		</div>
	</div>
</section>


{% endblock %}

{% block javascripts %}

{% endblock %}

```

</TabItem>
</Tabs>


### Création de la page erreur 403

Ouvrez le fichier "error403.html.twig" et collez le code suivant : 

<Tabs groupId="operating-systems">
  <TabItem value="new" label="VS Code">

```html

{% block stylesheets %}
	<link rel="stylesheet" href="https://bootswatch.com/5/united/bootstrap.min.css">
 

{% endblock %}
{% block body %}
<section class="page_404">
	<div class="container-fluid">
		<div class="row">	
		<div class="col-sm-12 col-md-12 col-lg-12 ">
		<div class="col-sm-12 col-sm-offset-1  text-center">
		<div class="four_zero_four_bg">
			<h1 class="text-center py-5">Error 403</h1>
		
        <img src="https://media.giphy.com/media/njYrp176NQsHS/giphy.gif" alt="gandalf_forbidden" srcset="">

		
		</div>
		
		<div class="contant_box_404">
		<h3 class="h2">
		Il semblerait que vous soyez perdu !
		</h3>
		
		<p>Vous n'avez pas le droit d'emprunter ce chemin !</p>
		
		<a href=" {{ path("app_login") }}" class="link_404">Faire demi-tour le plus rapidement possible</a>
	</div>
		</div>
		</div>
		</div>
	</div>
</section>


{% endblock %}

{% block javascripts %}

{% endblock %}

```

</TabItem>
</Tabs>

### Pour toutes les autres erreurs : 

Ouvrez le fichier "error.html.twig" et collez le code suivant : 

<Tabs groupId="operating-systems">
  <TabItem value="new" label="VS Code">

```html

{% block stylesheets %}
	 <link rel="stylesheet" href="https://bootswatch.com/5/united/bootstrap.min.css"> 
 

{% endblock %}
{% block body %}
<section class="page_404">
	<div class="container-fluid">
		<div class="row">	
		<div class="col-sm-12 col-md-12 col-lg-12 ">
		<div class="col-sm-12 col-sm-offset-1  text-center">
		<div class="four_zero_four_bg">
			<h1 class="text-center py-5">Erreur Inconnue !</h1>
		
        <img src="https://c.tenor.com/r5MTf8fGpwAAAAAC/flashlight-mort.gif" alt="" srcset="">
		
		</div>
		
		<div class="contant_box_404">
		<h3 class="h2">
		Quelque chose est cassé, mais on ne sait pas quoi
		</h3>
		
		<p>Notre équipe travaille dessus afin de réparer le problème...</p>
		
		<a href=" {{ path("app_login") }}" class="link_404">Retour à l'accueil</a>
	</div>
		</div>
		</div>
		</div>
	</div>
</section>


{% endblock %}

{% block javascripts %}

{% endblock %}

```
</TabItem>
</Tabs>


## Comment tester les pages d'erreur en environnement dev

Dans le dossier config/routes/ ouvrez le fichier framework.yaml et copiez le code suivant :


<Tabs groupId="operating-systems">
  <TabItem value="new" label="VS Code">

```init
# config/routes/framework.yaml
when@dev:
    _errors:
        resource: '@FrameworkBundle/Resources/config/routing/errors.xml'
        prefix:   /_error
```

</TabItem>
</Tabs>

### Réaliser les tests

Dans l'URL tapez l'adresse suivante, en remplacant par le code erreur souhaité

- http://127.0.0.1:8000/_error/{statusCode}




## Résultat

Erreur 404 :
![404](../img/404.png)

Erreur 403 :
![403](../img/403.png)

Autres erreurs : 
![autres](../img/autres.png)
 

## Message Flash

![flash](http://fairedesgifs.free.fr/da/sh/flash/flash%20(10).gif)

:::note précisions

Le but est d'afficher des messages informatifs pour l'utilisateurs selon les actions qu'il a entrepris

:::

### Ajout d'un message dans un Controller

Si vous voulez ajouter un message Flash dans un controller, avant le renvoi de vue, ajouter le code suivant :

```php
  $this->addFlash(
                    'votre type de flash (danger, success etc..',
                    'Votre message');

```

Exemple pour l'ajout de ticket ou la modification de ticket dans le fichier TicketController.php
```php
if ($title == 'Création d\'un ticket') {
//This will success
                $this->addFlash(
                    //This will success
                    'success',
                    //This will success
                    'Votre ticket à bien été ajouté');
            } else {


```

Fonction complète :

```php
    /**
     * @Route("/create", name="ticket_create")
     * @Route("/update/{id}", name="ticket_update", requirements={"id"="\d+"})
     */
    public function ticket(Ticket $ticket = null, Request $request)
    {
        if (!$ticket) {
        $ticket = new Ticket;

        $ticket->setIsActive(true)
            ->setCreateAt(new \DateTimeImmutable());
        
        $title = 'Création d\'un ticket';
        }else {
            $title = "Update du formulaire : {$ticket->getId()}";

        }
        
        $form = $this->createForm(TicketType::class, $ticket, []);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $this->ticketRepository->add($ticket, true);

            if ($title == 'Création d\'un ticket') {
// This will success
                $this->addFlash(
                    // This will success
                    'success',
                    // This will success
                    'Votre ticket a bien été ajouté');
                    
            } else {
// This will success
                $this->addFlash(
                    // This will success
                    'info',
                    // This will success
                    'Votre ticket a bien été mis à jour');
            }

            return $this->redirectToRoute('app_ticket');
        }
       return $this->render("ticket/userForm.html.twig", [
            "form" => $form->createView(),
            'title' => $title
        ]);
    }
```

## Rendu final :

### Mise à jour d'un ticket (Flash info) :
![flashInfo](../img/flashinfo.png)

### Suppression d'un ticket (Flash warning) :
![flashWarning](../img/flashwarning.png)

### Ajout d'un ticket (Flash success) :
![flashSuccess](../img/flashsuccess.png)


# <span className="red-text">Ce tutoriel vous a été présenté par l'Équipe Nicolas et Maxime d'un MaxDeFormation.com</span>