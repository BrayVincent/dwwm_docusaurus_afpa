---
sidebar_position: 11
title: Export
---

# Export PDF / Excel

@Author Vanessa, Thibault

## Export PDF

<br />

### Pré-requis

Dans cet exemple, nous allons convertir une page HTML en PDF grâce à un convertisseur, Dompdf . Nous allons générer à partir d'un controleur un fichier PDF qui sera téléchargé automatiquement.
Notre fichier HTML sera stocké dans un fichier Twig situé qui générera par la suite le fichier PDF.  
<br />

### Installation

Installer le Bundle DomPDF

```bash
composer require dompdf/dompdf
```

<br />

### Création du fichier Twig

Créer un fichier Twig à l'emplacement suivant : _project/templates/ticket/pdf.html.twig_
et insérer le code suivant :

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Export PDF</title>
</head>
<body>
    <div class="container">

    <h1 class="my-3">Liste des tickets</h1>

    <table class="table table-striped tabled-bordered" id="myTable">
        <thead>
            <tr>
                <th>ID</th>
                <th>Object</th>
                <th>Created At</th>
                <th>Department</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            {%for ticket in tickets %}
                <tr>
                    <th scope ="row">{{ ticket.id }}</th>
                    <td>{{ ticket.object }}</td>
                    <td>{{ ticket.createdAt | date('d/m/Y H:i')}}</td>
                    <td>{{ ticket.department.name }}</td>
                    <td>{{ ticket.TicketStatut }}</td>
                </tr>
            {% endfor %}
        </tbody>
    </table>
</div>
    </div>
</body>
</html>
```

<br />

### Création de la fonction pdf

Dans le TicketController, nous allons créer une fonction permettant de générer le PDF :

```php
 /**
 * @Route("/pdf", name="ticket_pdf")
 */
public function pdf() : Response
{
//Données utiles
$user = $this->getUser();
$tickets = $this->ticketRepository->findBy(['user' => $user] );

    //Configuration de Dompdf
$pdfOptions = new Options();
$pdfOptions->set('defaultFont', 'Arial');

//Instantiation de Dompdf
$dompdf = new Dompdf($pdfOptions);

//Récupération du contenu de la vue
$html = $this->renderView('ticket/pdf.html.twig', [
    'tickets' => $tickets,
    'title' => "Bienvenue sur notre page PDF"
]);

//Ajout du contenu de la vue dans le PDF
$dompdf->loadHtml($html);

//Configuration de la taille et de la largeur du PDF
$dompdf->setPaper('A4', 'portrait');

//Récupération du PDF
$dompdf->render();

//Création du fichier PDF
$dompdf->stream("ticket.pdf", [
    "Attachment" => true
]);

return new Response ('', 200, [
    'Content-Type' => 'application/pdf'
]);
}
```

### Création d'un bouton

<br />
Maintenant que notre fonction et notre page sont prêtes, il nous reste qu'à créer le bouton dans le menu de navigation.
<br />
Dans le fichier _projet/templates/ticket/index.html.twig_, nous allons ajouter le bouton suivant :

```
<a href="{{ path('ticket_pdf')}}" class="btn btn-danger">Export PDF</a>
```

<br />
<hr />

## Export Excel

<br />

### Pré-requis

Nous allons importer les données de la page HTML en format .xlsx pour pouvoir les exploiter dans un tableur.

### Installation

Commençons par installer la librairie PHPSpreadsheet :

```bash
composer require phpoffice/phpspreadsheet
```

Comme nous voulons télécharger le fichier Excel aussitôt qu'il est généré, nous aurons aussi besoin d'importer un composant supplémentaire, MIME.

> MIME (Multipurpose Internet Mail Extensions) est une norme Internet qui étend le format de base original des e-mails pour prendre en charge des fonctionnalités telles que :
>
> - En-têtes et contenu textuel utilisant des caractères non ASCII ;
> - Corps de message en plusieurs parties (par exemple, contenu HTML et texte brut) ;
> - Pièces jointes non textuelles : audio, vidéo, images, PDF, etc.
>   L'ensemble du standard MIME est complexe et énorme, mais Symfony résume toute cette complexité pour fournir deux façons de créer des messages MIME :
> - Une API de haut niveau basée sur la classe Email pour créer rapidement des e-mails avec toutes les fonctionnalités communes ;
> - Une API de bas niveau basée sur la classe Message pour avoir un contrôle absolu sur chaque partie du message électronique.

<br/>
Pour cela, nous allons l'installer grâce à Composer :

```bash
composer require symfony/mime
```

### Création de la fonction

Dans notre TicketController, nous allons commencer par créer la route de notre fonction :

```php
    /**
     * @Route("/excel", name="ticket_excel")
     */
```

<br />
Nous allons ensuite créer la fonction qui va générer le fichier Excel :

```php
   public function excel() : Response {

            //Données utiles
            $user = $this->getUser();
            $tickets = $this->ticketRepository->findBy(['user' => $user] );


            $spreadsheet = new Spreadsheet ();
            $sheet = $spreadsheet->getActiveSheet ();
            $sheet->setCellValue ('A1', 'Liste des tickets pour l\'utilisateur : ' . $user->getUsername());
            $sheet->mergeCells('A1:E1');
            $sheet->setTitle("Liste des tickets");

            //Set Column names
            $columnNames = [
                'Id',
                'Objet',
                'Date de création',
                'Department',
                'Statut',
            ];
            $columnLetter = 'A';
            foreach ($columnNames as $columnName) {
                $sheet->setCellValue ($columnLetter . '3', $columnName);
                $sheet->getColumnDimension($columnLetter)->setAutoSize(true);
                $columnLetter++;
            }
             foreach ($tickets as $key => $ticket) {
                $sheet->setCellValue ('A' . ($key + 4), $ticket->getId());
                $sheet->setCellValue ('B' . ($key + 4), $ticket->getObject());
                $sheet->setCellValue ('C' . ($key + 4), $ticket->getCreatedAt()->format('d/m/Y'));
                $sheet->setCellValue ('D' . ($key + 4), $ticket->getDepartment()->getName());
                $sheet->setCellValue ('E' . ($key + 4), $ticket->getTicketStatut());
            }
            //Création du fichier xlsx
            $writer = new Xlsx($spreadsheet);

            //Création d'un fichier temporaire
            $fileName = "Export_Tickets.xlsx";
            $temp_file = tempnam(sys_get_temp_dir(), $fileName);

            //créer le fichier excel dans le dossier tmp du systeme
            $writer->save($temp_file);

            //Renvoie le fichier excel
            return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
     }

```

Pour rendre le fichier moins brut, nous allons lui donner un peu plus de style :

```php
// -- Style de la feuille de calcul --
$styleArrayHead = [
    'font' => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
        'vertical' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];

$styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

$styleArrayBody = [
    'alignement' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
        'vertical' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
        'horizontal' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];

$sheet->getStyle('A1:E1')->applyFromArray($styleArray);
$sheet->getStyle('A3:E3')->applyFromArray($styleArrayHead);
$sheet->getStyle('A4:E' . (count($tickets) + 3))->applyFromArray($styleArrayBody);

//Création du fichier xlsx
$writer = new Xlsx($spreadsheet);

```

<br />

### Création du bouton

Dans le fichier _projet/templates/ticket/index.html.twig_, nous allons ajouter le bouton suivant :

```
<a href="{{ path('ticket_excel')}}" class="btn btn-success">Export Excel</a>
```

<br />
